//
//  Array.swift
//  whaddup
//
//  Created by Jin Hong on 2018-08-17.
//  Copyright © 2018 whatup. All rights reserved.
//

import Foundation

extension Array {
    func get(_ i: Int) -> Element? {
        guard i >= 0 && i < count else { return nil }
        return self[i]
    }
}

extension Array where Element == StringLiteralType {
    init(takingUnique array: [Element]) {
        self = Array(Set(array))
    }
}
